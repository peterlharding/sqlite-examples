using System;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace SQLite
{
    /// <summary>
    /// Summary description for MainForm.
    /// </summary>
    public class MainForm : Form
    {
        private int _id;

        private readonly DataSet _ds = new DataSet();
        private DataTable _dt = new DataTable();

        private DataGrid _grid;

        // ReSharper disable InconsistentNaming
        private Label Lbl_Desc;
        private Label Lbl_Id;
        private Button Btn_Delete;
        private Button Btn_Add;
        private Button Btn_Save;
        private Button Btn_Exit;
        private TextBox Txt_Id;
        private TextBox Txt_Desc;
        // ReSharper restore InconsistentNaming

        //-----------------------------------------------------------------------------------------

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private readonly Container _components = null;

        //-----------------------------------------------------------------------------------------

        public MainForm()
        {
            InitializeComponent();

        } // MainForm

        //-----------------------------------------------------------------------------------------

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_components != null)
                {
                    _components.Dispose();
                }
            }

            base.Dispose(disposing);

        } // Dispose

        //-----------------------------------------------------------------------------------------

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._grid = new System.Windows.Forms.DataGrid();
            this.Txt_Desc = new System.Windows.Forms.TextBox();
            this.Btn_Delete = new System.Windows.Forms.Button();
            this.Btn_Add = new System.Windows.Forms.Button();
            this.Lbl_Desc = new System.Windows.Forms.Label();
            this.Btn_Save = new System.Windows.Forms.Button();
            this.Btn_Exit = new System.Windows.Forms.Button();
            this.Lbl_Id = new System.Windows.Forms.Label();
            this.Txt_Id = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            this.SuspendLayout();
            // 
            // _grid
            // 
            this._grid.AlternatingBackColor = System.Drawing.Color.WhiteSmoke;
            this._grid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._grid.BackColor = System.Drawing.Color.Gainsboro;
            this._grid.BackgroundColor = System.Drawing.Color.DarkGray;
            this._grid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._grid.CaptionBackColor = System.Drawing.Color.DarkKhaki;
            this._grid.CaptionFont = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this._grid.CaptionForeColor = System.Drawing.Color.Black;
            this._grid.DataMember = "";
            this._grid.FlatMode = true;
            this._grid.Font = new System.Drawing.Font("Times New Roman", 9F);
            this._grid.ForeColor = System.Drawing.Color.Black;
            this._grid.GridLineColor = System.Drawing.Color.Silver;
            this._grid.HeaderBackColor = System.Drawing.Color.Black;
            this._grid.HeaderFont = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this._grid.HeaderForeColor = System.Drawing.Color.White;
            this._grid.LinkColor = System.Drawing.Color.DarkSlateBlue;
            this._grid.Location = new System.Drawing.Point(12, 12);
            this._grid.Name = "_grid";
            this._grid.ParentRowsBackColor = System.Drawing.Color.LightGray;
            this._grid.ParentRowsForeColor = System.Drawing.Color.Black;
            this._grid.PreferredColumnWidth = 120;
            this._grid.PreferredRowHeight = 25;
            this._grid.ReadOnly = true;
            this._grid.SelectionBackColor = System.Drawing.Color.Firebrick;
            this._grid.SelectionForeColor = System.Drawing.Color.White;
            this._grid.Size = new System.Drawing.Size(536, 376);
            this._grid.TabIndex = 0;
            this._grid.TabStop = false;
            this._grid.Click += new System.EventHandler(this.Grid_Click);
            // 
            // Txt_Desc
            // 
            this.Txt_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Txt_Desc.Location = new System.Drawing.Point(125, 398);
            this.Txt_Desc.Name = "Txt_Desc";
            this.Txt_Desc.Size = new System.Drawing.Size(423, 20);
            this.Txt_Desc.TabIndex = 1;
            // 
            // Btn_Delete
            // 
            this.Btn_Delete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Delete.Enabled = false;
            this.Btn_Delete.Location = new System.Drawing.Point(442, 424);
            this.Btn_Delete.Name = "Btn_Delete";
            this.Btn_Delete.Size = new System.Drawing.Size(50, 23);
            this.Btn_Delete.TabIndex = 4;
            this.Btn_Delete.Text = "Delete";
            this.Btn_Delete.Click += new System.EventHandler(this.Btn_Del_Click);
            // 
            // Btn_Add
            // 
            this.Btn_Add.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Add.Location = new System.Drawing.Point(346, 424);
            this.Btn_Add.Name = "Btn_Add";
            this.Btn_Add.Size = new System.Drawing.Size(42, 23);
            this.Btn_Add.TabIndex = 2;
            this.Btn_Add.Text = "Add";
            this.Btn_Add.Click += new System.EventHandler(this.Btn_Add_Click);
            // 
            // Lbl_Desc
            // 
            this.Lbl_Desc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Lbl_Desc.Location = new System.Drawing.Point(85, 401);
            this.Lbl_Desc.Name = "Lbl_Desc";
            this.Lbl_Desc.Size = new System.Drawing.Size(34, 20);
            this.Lbl_Desc.TabIndex = 0;
            this.Lbl_Desc.Text = "Desc";
            // 
            // Btn_Save
            // 
            this.Btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Save.Enabled = false;
            this.Btn_Save.Location = new System.Drawing.Point(394, 424);
            this.Btn_Save.Name = "Btn_Save";
            this.Btn_Save.Size = new System.Drawing.Size(42, 23);
            this.Btn_Save.TabIndex = 3;
            this.Btn_Save.Text = "Save";
            this.Btn_Save.Click += new System.EventHandler(this.Btn_Save_Click);
            // 
            // Btn_Exit
            // 
            this.Btn_Exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Exit.Location = new System.Drawing.Point(498, 424);
            this.Btn_Exit.Name = "Btn_Exit";
            this.Btn_Exit.Size = new System.Drawing.Size(50, 23);
            this.Btn_Exit.TabIndex = 5;
            this.Btn_Exit.Text = "Exit";
            this.Btn_Exit.Click += new System.EventHandler(this.Btn_Exit_Click);
            // 
            // Lbl_Id
            // 
            this.Lbl_Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Lbl_Id.Location = new System.Drawing.Point(12, 401);
            this.Lbl_Id.Name = "Lbl_Id";
            this.Lbl_Id.Size = new System.Drawing.Size(18, 20);
            this.Lbl_Id.TabIndex = 0;
            this.Lbl_Id.Text = "Id";
            // 
            // Txt_Id
            // 
            this.Txt_Id.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.Txt_Id.Enabled = false;
            this.Txt_Id.Location = new System.Drawing.Point(36, 398);
            this.Txt_Id.Name = "Txt_Id";
            this.Txt_Id.ReadOnly = true;
            this.Txt_Id.Size = new System.Drawing.Size(43, 20);
            this.Txt_Id.TabIndex = 0;
            this.Txt_Id.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(560, 457);
            this.Controls.Add(this.Txt_Id);
            this.Controls.Add(this.Lbl_Id);
            this.Controls.Add(this.Btn_Exit);
            this.Controls.Add(this.Btn_Save);
            this.Controls.Add(this.Lbl_Desc);
            this.Controls.Add(this.Btn_Add);
            this.Controls.Add(this.Btn_Delete);
            this.Controls.Add(this.Txt_Desc);
            this.Controls.Add(this._grid);
            this.Name = "MainForm";
            this.Text = "SQLite Demo";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            Application.Run(new MainForm());
        } // Main

        //-----------------------------------------------------------------------------------------

        private void LoadData()
        {
            var sqlCon = SetConnection();

            sqlCon.Open();

            var sqlCmd = sqlCon.CreateCommand();

            var commandText = "select id, desc from  mains";

            var db = new SQLiteDataAdapter(commandText, sqlCon);

            _ds.Reset();

            db.Fill(_ds);

            _dt = _ds.Tables[0];

            _grid.DataSource = _dt;

            sqlCon.Close();

        } // LoadData

        //-----------------------------------------------------------------------------------------

        private SQLiteConnection SetConnection()
        {
            return new SQLiteConnection("Data Source=C:/Temp/Demo.sqlite;Version=3;New=False;Compress=True;");
        } // SetConnection

        //-----------------------------------------------------------------------------------------

        private void ExecuteQuery(string txtQuery)
        {
            var sqlCon = SetConnection();

            sqlCon.Open();

            var sqlCmd = sqlCon.CreateCommand();

            sqlCmd.CommandText = txtQuery;

            sqlCmd.ExecuteNonQuery();

            sqlCon.Close();

        } // ExecuteQuery

        //-----------------------------------------------------------------------------------------

        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadData();

        } // MainForm_Load

        //-----------------------------------------------------------------------------------------

        private void Grid_Click(object sender, EventArgs e)
        {
            _id = Convert.ToInt32(_dt.Rows[_grid.CurrentRowIndex]["id"]);

            Btn_Delete.Enabled = true;
            Btn_Save.Enabled = true;

            Txt_Id.Text = _id.ToString();
            Txt_Desc.Text = _dt.Rows[_grid.CurrentRowIndex]["desc"].ToString();

        } // Grid_Click

        //-----------------------------------------------------------------------------------------

        private void Add()
        {
            var txtSqlQuery = "insert into  mains (desc) values ('" + Txt_Desc.Text + "')";

            ExecuteQuery(txtSqlQuery);

            Txt_Id.Text = string.Empty;
            Txt_Desc.Text = string.Empty;

        } // Add

        //-----------------------------------------------------------------------------------------

        private void Delete()
        {
            var txtSqlQuery = "delete from  mains where id =" + _id;

            ExecuteQuery(txtSqlQuery);

            Txt_Desc.Text = string.Empty;

        } // Delete

        //-----------------------------------------------------------------------------------------

        private void Save()
        {
            var txtSqlQuery = "update  mains set  desc =\"" + Txt_Desc.Text + "\" where id =" + _id;

            ExecuteQuery(txtSqlQuery);

        } // Save

        //-----------------------------------------------------------------------------------------

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Add();

            LoadData();

            Btn_Save.Enabled = false;

            Txt_Desc.Text = string.Empty;

        } // Btn_Add_Click
        
        //-----------------------------------------------------------------------------------------

        private void Btn_Del_Click(object sender, EventArgs e)
        {
            Delete();

            LoadData();

        } // Btn_Del_Click

        //-----------------------------------------------------------------------------------------

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            Save();

            LoadData();

        } // Btn_Save_Click

        //-----------------------------------------------------------------------------------------

        private void Btn_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();

        } // Btn_Exit_Click

        //-----------------------------------------------------------------------------------------

    }

}
