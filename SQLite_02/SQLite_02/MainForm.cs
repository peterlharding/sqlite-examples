﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;


namespace SQLite_02
{
    public partial class MainForm : Form
    {
        private int _id;

        private readonly DataSet _ds = new DataSet();
        private DataTable _dt = new DataTable();


        //-----------------------------------------------------------------------------------------

        public MainForm()
        {
            InitializeComponent();

            // DGV_Display.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells
        }

        //-----------------------------------------------------------------------------------------

        private void MainForm_Load(object sender, EventArgs e)
        {
            LoadData();

            DGV_Display.AllowUserToAddRows = false;

            DGV_Display.Columns[0].Width = 30;

        } // MainForm_Load

        //=========================================================================================

        #region - Data Handlers

        //-----------------------------------------------------------------------------------------

        private void LoadData()
        {
            var sqlCon = SetConnection();

            sqlCon.Open();

            var sqlCmd = sqlCon.CreateCommand();

            const string commandText = "select id, desc from  mains";

            var db = new SQLiteDataAdapter(commandText, sqlCon);

            _ds.Reset();

            db.Fill(_ds);

            _dt = _ds.Tables[0];

            DGV_Display.DataSource = _dt;

            sqlCon.Close();



        } // LoadData

        //-----------------------------------------------------------------------------------------

        private SQLiteConnection SetConnection()
        {
            return new SQLiteConnection("Data Source=C:/Temp/Demo.sqlite;Version=3;New=False;Compress=True;");

        } // SetConnection

        //-----------------------------------------------------------------------------------------

        private void ExecuteQuery(string txtQuery)
        {
            var sqlCon = SetConnection();

            sqlCon.Open();

            var sqlCmd = sqlCon.CreateCommand();

            sqlCmd.CommandText = txtQuery;

            sqlCmd.ExecuteNonQuery();

            sqlCon.Close();

        } // ExecuteQuery

        //-----------------------------------------------------------------------------------------

        private void Grid_Click(object sender, EventArgs e)
        {
            //int idx = e.RowIndex;

            //_id = Convert.ToInt32(_dt.Rows[DGV_Display.CurrentRowIndex]["id"]);

            Btn_Delete.Enabled = true;
            Btn_Save.Enabled = true;

            //Txt_Id.Text = _id.ToString();

            //Txt_Description.Text = _dt.Rows[DGV_Display.CurrentRowIndex]["desc"].ToString();

        } // Grid_Click

        //-----------------------------------------------------------------------------------------

        private void Add()
        {
            var txtSqlQuery = "insert into  mains (desc) values ('" + Txt_Description.Text + "')";

            ExecuteQuery(txtSqlQuery);

            Txt_Id.Text = string.Empty;
            Txt_Description.Text = string.Empty;

        } // Add

        //-----------------------------------------------------------------------------------------

        private void Delete()
        {
            var txtSqlQuery = "delete from  mains where id =" + _id;

            ExecuteQuery(txtSqlQuery);

            Txt_Description.Text = string.Empty;

        } // Delete

        //-----------------------------------------------------------------------------------------

        private void Save()
        {
            var txtSqlQuery = "update  mains set  desc =\"" + Txt_Description.Text + "\" where id =" + _id;

            ExecuteQuery(txtSqlQuery);

        } // Save

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------

        #endregion - Data Handlers

        //=========================================================================================

        #region - Button Handlers

        //-----------------------------------------------------------------------------------------

        private void Btn_Add_Click(object sender, EventArgs e)
        {
            Add();

            LoadData();

            Btn_Save.Enabled = false;

            Txt_Description.Text = string.Empty;

        } // Btn_Add_Click

        //-----------------------------------------------------------------------------------------

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            Save();

            LoadData();

        } // Btn_Save_Click

        //-----------------------------------------------------------------------------------------

        private void Btn_Delete_Click(object sender, EventArgs e)
        {
            Delete();

            LoadData();

        } // Btn_Delete_Click

        //-----------------------------------------------------------------------------------------

        private void Btn_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();

        } // Btn_Exit_Click

        //-----------------------------------------------------------------------------------------

        #endregion - Button Handlers

        //=========================================================================================


        private void DGV_Display_Loaded(object sender, NavigateEventArgs ne)
        {

        } // DGV_Display_Loaded

        //-----------------------------------------------------------------------------------------

        private void DGV_Display_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var rowIndex = e.RowIndex;

            var colIdx = e.ColumnIndex;

            if (rowIndex >= 0)
            {
                DataGridViewRow row = DGV_Display.Rows[rowIndex];

                //_id = Convert.ToInt32(_dt.Rows[rowIndex]["id"]);
                var x = row.Cells[0].Value;

                _id = Convert.ToInt32(x.ToString());

                Txt_Id.Text = _id.ToString();

                // Txt_Description.Text = _dt.Rows[rowIndex]["desc"].ToString();
                Txt_Description.Text = row.Cells[1].Value.ToString();

                Btn_Delete.Enabled = true;
                Btn_Save.Enabled = true;

                Refresh();
            }

        } // DGV_Display_CellContentClick

        //-----------------------------------------------------------------------------------------



    }
}
